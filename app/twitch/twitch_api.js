'use strict';

const https = require('https');
const requests = require('request');

class TwitchAPI
{
    static getStreamList(limit, callback)
    {
        // went from https get call to using request as to add a clientID per new policy of the API
		var options = {
		url: 'https://api.twitch.tv/helix/streams/?limit=' + limit ,
		headers: {'Client-ID': 'v6yazwwpv0h050b6y6ti2xfezgt638', 'Authorization': 'Bearer zpyxwaho3sso5bwnny1pe7hie7s0oc'}
		};
		
		function callB(error, response, body){
			if(error )
			{
				console.log(error);
				
			}
			else
			{
				var info = JSON.parse(body);
				callback(null, info);
				
			}
		}

		requests(options, callB);
       
    }

    
    static getTopGames(limit, callback)
    {
        // went from https get call to using request as to add a clientID per new policy of the API
		var options = {
		url: 'https://api.twitch.tv/helix/games/top?limit='+limit ,
		headers: {'Client-ID': 'v6yazwwpv0h050b6y6ti2xfezgt638', 'Authorization': 'Bearer zpyxwaho3sso5bwnny1pe7hie7s0oc'}
		};
		

		function callB(error, response, body){
			if(error )
			{
				console.log(error);
				
			}
			else
			{
				var info = JSON.parse(body);
				callback(null, info);
				
			}
		}

		requests(options, callB);
            
    }
    
    static getTopStreamsByGame(game, limit, callback)
    {
        // went from https get call to using request as to add a clientID per new policy of the API
		var options = {
		url: 'https://api.twitch.tv/helix/streams/?game=' + game  + '&limit=' + limit ,
		headers: {'Client-ID': 'v6yazwwpv0h050b6y6ti2xfezgt638', 'Authorization': 'Bearer zpyxwaho3sso5bwnny1pe7hie7s0oc'}
		};

		function callB(error, response, body){
			if(error )
			{
				console.log(error);
				
			}
			else
			{
				var info = JSON.parse(body);
				callback(null, info);
				
			}
		}

		requests(options, callB);
        
       
    }
};

module.exports = TwitchAPI;